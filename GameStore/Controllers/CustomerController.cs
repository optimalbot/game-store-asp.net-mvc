﻿using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameStore.Controllers
{
    public class CustomerController : Controller
    {

        //DbContext is a disposable object see Dispose()

        private ApplicationDbContext _context;
        
        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }
        

        public ActionResult Index()
        {
            //ToList is because the context does not query the db until we iterate over the context object. This forces it to become an enumerable object before
            //passing it into the view

         
            

            var customers = _context.Customers.ToList();

            return View(customers);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
    }
}